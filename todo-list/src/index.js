import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import TodoList from "./Components/TodoList";
import Header from "./Components/Header";
import AddTodo from "./Components/AddTodo";
import { BrowserRouter as MyRouter, Route } from "react-router-dom";
import About from "./Components/pages/About"
// all the imports required to do some of the work


/* This is the main class, or component, where all components go in to. 
The idea is to keep one component simply for rendering the logic other components produce.
Following the idea of single responsibility */
class Main extends React.Component {
  /*
here we have an array named todos. it contains some of our todo items,
like cook pasta. yummy. anyway, it is created in the Main class.
In our render method, we are going to pass this array over to another class,
or component, whatever you name it. This component will be Todos.
Todos will be acting like our container for the todo items. 
The array is passed on using props, which is simply a fancy way of saying arguments.

Anyway, head on over to Todos.js to see what happens next
*/
  state = {
    todo_items: [
      {
        id: 1,
        title: "Cook some pasta",

        completed: false,
      },
      {
        id: 2,
        title: "Make breakfast",

        completed: false,
      },
      {
        id: 3,
        title: "Make some green beans",

        completed: false,
      },
    ],
    count: 4,
  };

  // if the checkbox is marked, the to-do
  // item is marked as completed. This puts a line through the text
  ifChecked = (id) => {
    this.setState({
      todo_items: this.state.todo_items.map((item) => {
        if (item.id === id) {
          item.completed = !item.completed;
        }
        return item;
      }),
    });
  };
  /* This is the function which deletes a todo item */
  delItem = (id) => {
    this.setState({
      todo_items: [...this.state.todo_items.filter((item) => item.id !== id)],
    });
  };

  // this is the function which will add a todo item
  addTodo = (title, id) => {
    const newTodo = {
      id: this.state.count,
      title,
      completed: false,
    };

    this.setState({ todo_items: [...this.state.todo_items, newTodo] });
    this.state.count = this.state.count + 1;
  };

  render() {
    console.log(this.state.todo_items);
    return (
      <MyRouter>
        <Header />
        <Route exact path = "/"
        render = {props => (
                  <React.Fragment>
          <TodoList
            todo_items={this.state.todo_items}
            ifChecked={this.ifChecked}
            delItem={this.delItem}
          />
          <AddTodo addTodo={this.addTodo} />
        </React.Fragment>
        


        ) }
        />
        <Route path = "/about" component = {About} />

      </MyRouter>
    );
  }
}

ReactDOM.render(<Main />, document.getElementById("root"));
