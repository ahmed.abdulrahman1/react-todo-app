import React, { Component } from "react";
import styled from "styled-components";


export class AddTodo extends Component {

    state = {
        title: ""
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.props.addTodo(this.state.title);
        this.setState({title: ""})

    }

    onChange = (event) => this.setState({title: event.target.value})

    render() {
    return (
      <MyForm onSubmit = {this.onSubmit} >
        <TextInput type="text" name="title" placeholder="Add a to-do item..."
        value = {this.state.title}
        onChange = {this.onChange} />

        <SubmitButton type="submit" value="Submit" className="btn" />
      </MyForm>
    );
  }
}

const MyForm = styled.form`
  display: flex;
  width: 50%;
`;
const TextInput = styled.input`
  flex: 3;
  padding: 5px;
`;

const SubmitButton = styled.input`
  flex: 1;
`;

export default AddTodo;
