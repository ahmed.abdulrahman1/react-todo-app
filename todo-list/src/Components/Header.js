import React, { Component } from 'react'
import styled from 'styled-components'

export class Header extends Component {
    render() {
        return (
            <div>
                <MyHeader>To Do app</MyHeader>
            </div>
        )
    }
}

const MyHeader = styled.h1`
text-align: center;
padding:10px;

`

export default Header
