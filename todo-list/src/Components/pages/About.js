import React, { Component } from 'react'

export class About extends Component {
    render() {
        return (
            <React.Fragment>
                <h1>About page</h1>
                <p>Hello and welcome to my to-do created in React!. Enjoy your stay</p>
            </React.Fragment>
        )
    }
}

export default About
