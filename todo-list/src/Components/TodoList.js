import React, { Component } from "react";
import TodoItem from "./TodoItem";
import PropTypes from "prop-types";

// my container for the todo items
export class TodoList extends Component {
  render() {
    return this.props.todo_items.map((todo_item) => (
      <TodoItem
        key={todo_item.id}
        todo_item={todo_item}
        ifChecked={this.props.ifChecked}
        delItem = {this.props.delItem}
      />
    ));
  }
}

/* 
Here I am only allowing an array type to
be passed as a props for the TodoList.
If anything else is passed, an error 
will pop up, letting you know that you
are passing the wrong type.

Very interesting... I wonder if there's
a language out there that is strongly 
typed
*/
TodoList.propTypes = {
  todo_items: PropTypes.array.isRequired,
};

export default TodoList;
