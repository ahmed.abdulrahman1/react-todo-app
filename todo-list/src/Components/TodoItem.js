import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from 'styled-components'


//this can be viewed as our model. The individual item
export class TodoItem extends Component {
  getStyle = () => {
    // if the todo item is completed, put a line through it
    if (this.props.todo_item.completed) {
      return {
        textDecoration: "line-through",
      };
    }
  };



  render() {
    const { id, title } = this.props.todo_item
    return (
      <div style={this.getStyle()}>
        <h3>
          <input type = "checkbox" onChange = {this.props.ifChecked
          .bind(this, id)}/>
          {" "}
          { id}. { title}
          <MyButton onClick ={this.props.delItem.bind(this, id)} >Delete
          </MyButton>
        </h3>
      </div>
    );
  }
}

TodoItem.propTypes = {
  todo_item: PropTypes.object.isRequired,
};

const MyButton = styled.button`
background: red;
color: white;
padding: 5px 20px;
margin: 10px;

`

export default TodoItem;
